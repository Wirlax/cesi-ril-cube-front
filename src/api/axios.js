const axios = require('axios');
const api = axios.create({
    baseURL: 'https://cubecesi.online',
    timeout:1000,
})
export default api;