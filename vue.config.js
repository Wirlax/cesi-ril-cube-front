const webpack = require('webpack')

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? process.env.VUE_APP_BASE_URL
    : '/',

  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        mapboxgl: 'mapbox-gl'
      })
    ]
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  outputDir: 'html',
  pwa:{
    name: 'Ressources Relationelles',
    themeColor: '#6BC1FF',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'GenerateSW',
    iconPaths:{
      favicon32:"./src/assets/images/favicon-32x32.png",
      favicon16:"./src/assets/images/favicon-16x16.png",
      appleTouchIcon: './src/assets/images/apple-touch-icon-152x152.png',
      maskIcon: './src/assets/images/safari-pinned-tab.svg',
      msTileImage: './src/assets/images/msapplication-icon-144x144.png'
    },
  }
}
